// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownBomber.h"
#include "TDBGameInstance.h"
#include "TopDownBomberGameState.h"
#include "TopDownBomberPlayerState.h"


void UTDBGameInstance::SaveAllPersistentData()
{
	for (auto PlayerState : GetWorld()->GetGameState<ATopDownBomberGameState>()->PlayerArray)
	{
		ATopDownBomberPlayerState* CastPlayerState = Cast<ATopDownBomberPlayerState>(PlayerState);

		auto PlayerSaveData = FPlayerDataStruct(CastPlayerState);
		PlayerDataArray.Add(PlayerSaveData);
		//UE_LOG(LogTemp, Warning, TEXT("ID: %d, Number: %d, Score: %d"), PlayerSaveData.PlayerId, PlayerSaveData.PlayerNumber, PlayerSaveData.PlayerScore);
	}
}

void UTDBGameInstance::LoadAllPersistentData()
{
	for (auto PlayerState : GetWorld()->GetGameState<ATopDownBomberGameState>()->PlayerArray)
	{
		auto CastPlayerState = Cast<ATopDownBomberPlayerState>(PlayerState);

		for (auto PlayerSaveData : PlayerDataArray)
		{
			if (PlayerSaveData.PlayerId == CastPlayerState->PlayerId)
			{
				CastPlayerState->PlayerNumber = PlayerSaveData.PlayerNumber;
				CastPlayerState->PlayerScore = PlayerSaveData.PlayerScore;
			}
		}
	}
}
