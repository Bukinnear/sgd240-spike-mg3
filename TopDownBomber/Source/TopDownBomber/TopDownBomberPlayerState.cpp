// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownBomber.h"
#include "TopDownBomberPlayerState.h"
#include "UnrealNetwork.h"
#include "TopDownBomberPlayerController.h"

ATopDownBomberPlayerState::ATopDownBomberPlayerState()
{
	bCanRefreshUI = false;
	PlayerNumber = 0;
	PlayerScore = 0;
}


void ATopDownBomberPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATopDownBomberPlayerState, PlayerNumber);
	DOREPLIFETIME(ATopDownBomberPlayerState, PlayerScore);
	DOREPLIFETIME(ATopDownBomberPlayerState, bCanRefreshUI);
}

void ATopDownBomberPlayerState::OnRep_PlayerScore()
{
	if (bCanRefreshUI)
	{
		Cast<ATopDownBomberPlayerController>(GetWorld()->GetFirstPlayerController())->RefreshScoreUI(this);
	}
}
