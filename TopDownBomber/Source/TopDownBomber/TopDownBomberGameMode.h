// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "TopDownBomberGameMode.generated.h"

UCLASS(minimalapi)
class ATopDownBomberGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATopDownBomberGameMode();

	UFUNCTION()
		void OnPlayerDeath();

protected:

	UPROPERTY()
		FTimerHandle WinnerScreenDelay;

	UFUNCTION()
		void PostLogin(APlayerController* NewPlayer) override;

	UFUNCTION()
		void PostSeamlessTravel() override;

	UFUNCTION()
		int32 GetNumCurrentPlayers();

	UFUNCTION()
		void PostPlayerJoins();

	UFUNCTION()
		void TravelToNextMap();

	UFUNCTION()
		APlayerController* GetFirstLivingPlayer();

	// Player's number
	UPROPERTY()
		int NewPlayerNumber;

	UPROPERTY(BlueprintReadWrite)
		bool bCanBeginRound;

	UFUNCTION()
		virtual void InitDefaultClasses();

	UFUNCTION()
		void BeginCountdownOnAllPCs();

	UFUNCTION()
		void RemoveWidgetsFromAllPCs();

	UFUNCTION()
		void ShowWinnerScreenOnAllPCs(int WinningPlayerNumber);

public:
	UFUNCTION()
		void Debug();

	UFUNCTION()
		void Debug2();
};