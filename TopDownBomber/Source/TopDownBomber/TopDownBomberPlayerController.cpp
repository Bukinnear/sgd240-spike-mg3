// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TopDownBomber.h"
#include "TopDownBomberPlayerController.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "TopDownBomberCharacter.h"
#include "TopDownBomberGameState.h"
#include "TopDownBomberPlayerState.h"
#include "Blueprint/UserWidget.h"
#include "ScoreWidget.h"
#include "LoadingWidget.h"
#include "MainMenuWidget.h"
#include "DrawScreenWidget.h"
#include "WinnerScreenWidget.h"

//Delete later
#include "TopDownBomberGameMode.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>

ATopDownBomberPlayerController::ATopDownBomberPlayerController()
{
	bShowMouseCursor = true;
	CountdownIndex = 1;

	// Get our score widget UI class
	static ConstructorHelpers::FClassFinder<UUserWidget> ScoreWidgetObj(TEXT("/Game/TopDownCPP/Blueprints/ScoreWidget"));
	if (ScoreWidgetObj.Succeeded())
	{
		ScoreWidgetClass = ScoreWidgetObj.Class;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("ScoreWidget could not be found!")); }

	// Get our loading widget UI class
	static ConstructorHelpers::FClassFinder<UUserWidget> LoadingWidgetObj(TEXT("/Game/TopDownCPP/Blueprints/LoadingWidget"));
	if (LoadingWidgetObj.Succeeded())
	{
		LoadingWidgetClass = LoadingWidgetObj.Class;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("LoadingWidget could not be found!")); }	

	// Get our Main Menu widget UI class
	static ConstructorHelpers::FClassFinder<UUserWidget> MainMenuWidgetObj(TEXT("/Game/TopDownCPP/Blueprints/MainMenuWidget"));
	if (MainMenuWidgetObj.Succeeded())
	{
		MainMenuWidgetClass = MainMenuWidgetObj.Class;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("MainMenuWidget could not be found!")); }

	// Get our Draw Screen widget UI class
	static ConstructorHelpers::FClassFinder<UUserWidget> DrawScreenWidgetObj(TEXT("/Game/TopDownCPP/Blueprints/DrawScreenWidget"));
	if (DrawScreenWidgetObj.Succeeded())
	{
		DrawScreenWidgetClass = DrawScreenWidgetObj.Class;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("DrawScreenWidget could not be found!")); }

	// Get our Win Screen widget UI class
	static ConstructorHelpers::FClassFinder<UUserWidget> WinnerScreenWidgetObj(TEXT("/Game/TopDownCPP/Blueprints/WinnerScreenWidget"));
	if (WinnerScreenWidgetObj.Succeeded())
	{
		WinnerScreenWidgetClass = WinnerScreenWidgetObj.Class;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("WinnerScreenWidget could not be found!")); }
}

void ATopDownBomberPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void ATopDownBomberPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld()->GetMapName().Contains("MainMenuMap"))
	{
		ShowMainMenu();
	}
	else
	{
		ShowLoadingScreen();
	}
}

void ATopDownBomberPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	ATopDownBomberGameMode* OurGameMode = Cast<ATopDownBomberGameMode>(GetWorld()->GetAuthGameMode());

	InputComponent->BindAction("Debug", IE_Pressed, this, &ATopDownBomberPlayerController::Debug);
	InputComponent->BindAction("Debug2", IE_Pressed, this, &ATopDownBomberPlayerController::Debug2);
	if (OurGameMode->IsValidLowLevel())
	{
		InputComponent->BindAction("Debug3", IE_Pressed, OurGameMode, &ATopDownBomberGameMode::Debug);
	}
	InputComponent->BindAction("Debug4", IE_Pressed, OurGameMode, &ATopDownBomberGameMode::Debug2);
	
}

void ATopDownBomberPlayerController::MoveToMouseCursor()
{
	// Trace to see what is under the mouse cursor
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	if (Hit.bBlockingHit)
	{
		ATopDownBomberCharacter* OurPawn = Cast<ATopDownBomberCharacter>(GetPawn());
		if (OurPawn)
		{
			OurPawn->SetNewMoveDestination(Hit.ImpactPoint);
		}
	}	
}

void ATopDownBomberPlayerController::OnSetDestinationPressed()
{	
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;

	// Temporary code to prove that the score system works
	//ATopDownBomberPlayerState* OurPlayerState = Cast<ATopDownBomberPlayerState>(PlayerState);
	//int LocalScore = OurPlayerState->PlayerScore + 1;
	//SetOwnScoreUI(OurPlayerState->PlayerScore += 1);

	//Cast<ATopDownBomberCharacter>(GetPawn())->AddScore(OurPlayerState, 1);
}

void ATopDownBomberPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void ATopDownBomberPlayerController::DropBomb()
{
	// Tell the pawn to drop a bomb
	ATopDownBomberCharacter* OurPawn = Cast<ATopDownBomberCharacter>(GetPawn());
	if (OurPawn)
	{
		OurPawn->DropBomb();
	}
}

void ATopDownBomberPlayerController::AddNewScoreUI_Implementation(const ATopDownBomberPlayerState* NewPlayerState)
{
	// If the given PlayerState is not null
	if (NewPlayerState)
	{
		// and we are performing this on the local player controller
		if (IsLocalController())
		{
			// as long as we aren't already tracking this player's score
			if (!ScoreWidgetArray.Contains(NewPlayerState))
			{
				if (ScoreWidgetClass)
				{
					// Create a new widget and populate it's values
					UScoreWidget* PlayerScoreWidget = CreateWidget<UScoreWidget>(this, ScoreWidgetClass);

					if (PlayerScoreWidget)
					{
						PlayerScoreWidget->SetPlayerNumber(NewPlayerState->PlayerNumber);
						PlayerScoreWidget->SetPlayerScore(NewPlayerState->PlayerScore);

						// Add it to the viewport at an offset based on the player's number
						PlayerScoreWidget->AddToViewport();
						int x = 20;
						int y = 30 * NewPlayerState->PlayerNumber;
						FVector2D Offset = FVector2D(x, y);
						PlayerScoreWidget->SetPositionInViewport(Offset);

						// Add it to the list of players we are tracking
						ScoreWidgetArray.Add(NewPlayerState, PlayerScoreWidget);
					}
				}
			}
			else { UE_LOG(LogTemp, Warning, TEXT("This player has already been added to the UI ")); }
		}
	}
}

void ATopDownBomberPlayerController::SetOwnScoreUI_Implementation(int ScoreToSet)
{
	// get our player state & score widget
	ATopDownBomberPlayerState* OurPlayerState = Cast<ATopDownBomberPlayerState>(PlayerState);
	UScoreWidget* WidgetToRefresh = *ScoreWidgetArray.Find(OurPlayerState);

	// If that worked, set it to the new value
	if (WidgetToRefresh)
	{
		WidgetToRefresh->SetPlayerScore(ScoreToSet);
	}
	else { UE_LOG(LogTemp, Warning, TEXT("Widget is null")); }
}

void ATopDownBomberPlayerController::RefreshScoreUI_Implementation(ATopDownBomberPlayerState* PlayerStateToRefresh)
{
	// Get the relevant widget from the ScoreWidget array
	if (PlayerStateToRefresh->IsValidLowLevel())
	{
		UScoreWidget* WidgetToRefresh = *ScoreWidgetArray.Find(PlayerStateToRefresh);

		// If we get a value, set it to the new value
		if (WidgetToRefresh->IsValidLowLevel())
		{
			WidgetToRefresh->SetPlayerScore(PlayerStateToRefresh->PlayerScore);
		}
		else { UE_LOG(LogTemp, Warning, TEXT("Widget is null")); }
	}
	else { UE_LOG(LogTemp, Warning, TEXT("PlayerState is null")); }
}

void ATopDownBomberPlayerController::AddAllPlayerScores()
{
	// Get the PlayerStates array from the GameState
	ATopDownBomberGameState* GameState = Cast<ATopDownBomberGameState>(GetWorld()->GetGameState());
	TArray<APlayerState*> PlayerStates;
	if (GameState) { PlayerStates = GameState->PlayerArray; }
	else { UE_LOG(LogTemp, Warning, TEXT("Could not add scores: Could not get GameState")); }

	// If the Player array has values in it, continue
	if (PlayerStates.Num() > 0)
	{
		// Iterate through them (A range based loop doesn't work here >:C )
		for (int i = 0; i < PlayerStates.Num(); i++)
		{
			// Get the PlayerState at the current position
			ATopDownBomberPlayerState* CurrentPlayerState = Cast<ATopDownBomberPlayerState>(PlayerStates[i]);

			// If we got a value, add it to the UI
			if (CurrentPlayerState)
			{
				AddNewScoreUI(CurrentPlayerState);
			}
			else { UE_LOG(LogTemp, Warning, TEXT("Player %d: CurrentPlayerState is null at index: %d"), Cast<ATopDownBomberPlayerState>(PlayerState)->PlayerNumber, i); }
		}
	}
	else { UE_LOG(LogTemp, Warning, TEXT("PlayerArray was empty")); }
}

void ATopDownBomberPlayerController::RemoveAllPlayerScores_Implementation()
{
	for (auto ArrayPair : ScoreWidgetArray)
	{
		UScoreWidget* ScoreWidget = ArrayPair.Value;
		if (ScoreWidget->IsValidLowLevel())
		{
			ScoreWidget->RemoveFromViewport();
		}
	}
}

void ATopDownBomberPlayerController::ShowLoadingScreen_Implementation()
{
	// if we are performing this on the local player controller
	if (IsLocalController())
	{
		if (LoadingWidgetClass)
		{
			LoadingScreenWidget = CreateWidget<ULoadingWidget>(this, LoadingWidgetClass);			

			if (LoadingScreenWidget)
			{
				LoadingScreenWidget->AddToViewport();
				SetInputMode(FInputModeUIOnly());
			}
			else { UE_LOG(LogTemp, Warning, TEXT("Loading Screen Widget could not be created")); }
		}
		else { UE_LOG(LogTemp, Warning, TEXT("Loading Screen Widget class is null")); }
	}
}

void ATopDownBomberPlayerController::RemoveLoadingScreen_Implementation()
{
	if (LoadingScreenWidget)
	{
		LoadingScreenWidget->RemoveFromViewport();
		FInputModeGameAndUI InputMode;
		InputMode.SetHideCursorDuringCapture(false);
		SetInputMode(InputMode);
	}
}

void ATopDownBomberPlayerController::ShowMainMenu_Implementation()
{
	// if we are performing this on the local player controller
	if (IsLocalController())
	{
		if (MainMenuWidgetClass)
		{
			auto MainMenuWidget = CreateWidget<UMainMenuWidget>(this, MainMenuWidgetClass);

			if (MainMenuWidget)
			{
				MainMenuWidget->AddToViewport();
				SetInputMode(FInputModeUIOnly());
			}
			else { UE_LOG(LogTemp, Warning, TEXT("Main Menu Widget could not be created")); }
		}
		else { UE_LOG(LogTemp, Warning, TEXT("Main Menu Widget class is null")); }
	}
}

void ATopDownBomberPlayerController::ShowDrawScreen_Implementation()
{
	// if we are performing this on the local player controller
	if (IsLocalController())
	{
		if (DrawScreenWidgetClass)
		{
			auto DrawScreenWidget = CreateWidget<UDrawScreenWidget>(this, DrawScreenWidgetClass);

			if (DrawScreenWidget)
			{
				DrawScreenWidget->AddToViewport();
				SetInputMode(FInputModeUIOnly());
			}
			else { UE_LOG(LogTemp, Warning, TEXT("Draw Screen Widget could not be created")); }
		}
		else { UE_LOG(LogTemp, Warning, TEXT("Draw Screen Widget class is null")); }
	}
}

void ATopDownBomberPlayerController::ShowWinnerScreen_Implementation(int WinningPlayerNumber)
{
	// if we are performing this on the local player controller
	if (IsLocalController())
	{
		if (WinnerScreenWidgetClass)
		{
			WinnerScreenWidget = CreateWidget<UWinnerScreenWidget>(this, WinnerScreenWidgetClass);

			if (WinnerScreenWidget)
			{
				WinnerScreenWidget->SetWinningPlayerNumber(WinningPlayerNumber);
				WinnerScreenWidget->AddToViewport();
				SetInputMode(FInputModeUIOnly());
			}
			else { UE_LOG(LogTemp, Warning, TEXT("Main Menu Widget could not be created")); }
		}
		else { UE_LOG(LogTemp, Warning, TEXT("Main Menu Widget class is null")); }
	}
}

void ATopDownBomberPlayerController::RemoveWinnerScreen_Implementation()
{
	if (WinnerScreenWidget->IsValidLowLevel())
	{
		WinnerScreenWidget->RemoveFromViewport();
	}
}

void ATopDownBomberPlayerController::RoundCountdown_Implementation()
{
	// If we have a loading screen, change the text to indicate that the round is starting
	if (LoadingScreenWidget)
	{
		LoadingScreenWidget->SetCountdownText(CountdownIndex);
		LoadingScreenWidget->MatchIsStarting(true);
	}
	// If the coundown is not 0, decrement the coundown and run this function again
	if (CountdownIndex > 0)
	{		
		CountdownIndex--;
		GetWorldTimerManager().SetTimer(CountdownTimer, this, &ATopDownBomberPlayerController::RoundCountdown, 1.0f);
	}
	// If the coundown is 0, enable input, remove the loading screen, and notify the 
	else
	{
		// Change the cursor to crosshairs
		DefaultMouseCursor = EMouseCursor::Crosshairs;

		// Set up SetDestination action
		InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATopDownBomberPlayerController::OnSetDestinationPressed);
		InputComponent->BindAction("SetDestination", IE_Released, this, &ATopDownBomberPlayerController::OnSetDestinationReleased);

		// Set up BombDrop action
		InputComponent->BindAction("DropBomb", IE_Pressed, this, &ATopDownBomberPlayerController::DropBomb);

		// Remove the loading screen
		RemoveLoadingScreen();

		// add all of the player score to the UI if we haven't already done so
		if (ScoreWidgetArray.Num() < 1)
		{
			AddAllPlayerScores();
		}

		Cast<ATopDownBomberPlayerState>(PlayerState)->bCanRefreshUI = true;
	}
}

void ATopDownBomberPlayerController::RemoveAllWidgets_Implementation()
{
	RemoveAllPlayerScores();
	RemoveLoadingScreen();
	RemoveWinnerScreen();
}

void ATopDownBomberPlayerController::Debug()
{

}

void ATopDownBomberPlayerController::Debug2()
{
	Cast<ATopDownBomberCharacter>(GetPawn())->AddScore(1);
}

void ATopDownBomberPlayerController::Debug3()
{

}
