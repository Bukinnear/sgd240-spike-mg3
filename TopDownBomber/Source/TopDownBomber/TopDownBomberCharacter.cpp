// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TopDownBomber.h"
#include "TopDownBomberCharacter.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Bomb.h"
#include "UnrealNetwork.h"
#include "TopDownBomberPlayerState.h"
#include "TopDownBomberPlayerController.h"
#include "TopDownBomberGameMode.h"

ATopDownBomberCharacter::ATopDownBomberCharacter()
{
	// Initialize components
	InitComponents();

	// Number of bombs we can drop
	BombCount = 5;

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
		
	// Subscribe to the OnTakeDamage event to handle any incoming damage
	OnTakeAnyDamage.AddDynamic(this, &ATopDownBomberCharacter::OnTakeDamage);
}

void ATopDownBomberCharacter::InitComponents()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;	

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
}

void ATopDownBomberCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(ATopDownBomberCharacter, BombCount);	
}

void ATopDownBomberCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	// If the cursor decal is present
	if (CursorToWorld != nullptr)
	{
		// And we have a controller
		if (APlayerController* PC = Cast<APlayerController>(Controller))
		{
			// Move the decal to the cursor
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	// Move towards the destination if required
	if (bMoveToDestination)
	{
		MoveToDestination();
	}
}

void ATopDownBomberCharacter::SetNewMoveDestination(const FVector TargetLocation)
{
	// Set the bool + target to move
	bMoveToDestination = true;
	Destination = TargetLocation;
}

void ATopDownBomberCharacter::MoveToDestination()
{
	float Distance = FVector::Dist(Destination, GetActorLocation());
	FVector Location = GetActorLocation();

	// If the distance is large enough, move towards it
	if (Distance > 100)
	{
		FVector Movement = Destination - Location;
		// Normalize the vector, since AddMovementInput only moves in a given direction.
		Movement.Normalize(); 
		AddMovementInput(Movement);
	}
	// Otherwise, cancel moving to this destination
	else
	{
		bMoveToDestination = false;
	}
}

void ATopDownBomberCharacter::DropBomb_Implementation()
{
	if (HasAuthority() && BombCount > 0)
	{
		FVector BombSpawnLocation = GetActorLocation();
		FRotator BombSpawnRotation = GetActorRotation();

		BombSpawnLocation.Z += -100.0;

		ABomb* BombActor = GetWorld()->SpawnActor<ABomb>(BombSpawnLocation, BombSpawnRotation);
		BombActor->DamageCauseActor = this;
		BombCount--;
		UE_LOG(LogTemp, Warning, TEXT("%d Bombs remaining"), BombCount);
	}
}

bool ATopDownBomberCharacter::DropBomb_Validate()
{
	return true;
}

void ATopDownBomberCharacter::OnTakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser)
{
	// Processed by the server, only
	if (HasAuthority())
	{
		if (DamageCauser->IsValidLowLevel())
		{
			ATopDownBomberCharacter* DamageCauserCharacter = Cast<ATopDownBomberCharacter>(DamageCauser);
			if (DamageCauserCharacter->IsValidLowLevel())
			{
				DamageCauserCharacter->AddScore(1);
			}
			else { UE_LOG(LogTemp, Warning, TEXT("Actor is not a TDBCharacter")); }
		} 
		else { UE_LOG(LogTemp, Warning, TEXT("DamageCauser is not valid")); }
		Die();	
	}
}

void ATopDownBomberCharacter::Die_Implementation()
{
	PlayerState->bIsSpectator = true;
	auto OurGameMode = Cast<ATopDownBomberGameMode>(GetWorld()->GetAuthGameMode());
	if (OurGameMode->IsValidLowLevel())
	{
		OurGameMode->OnPlayerDeath();
	}
	Destroy();
}

bool ATopDownBomberCharacter::Die_Validate()
{
	return true;
}

void ATopDownBomberCharacter::AddScore_Implementation(int ScoreToAdd)
{
	ATopDownBomberPlayerState* OurPlayerState = Cast<ATopDownBomberPlayerState>(PlayerState);
	if (OurPlayerState->IsValidLowLevel())
	{
		OurPlayerState->PlayerScore += ScoreToAdd;
		Cast<ATopDownBomberPlayerController>(GetWorld()->GetFirstPlayerController())->RefreshScoreUI(OurPlayerState);
	}	
}

bool ATopDownBomberCharacter::AddScore_Validate(int ScoreToAdd)
{
	return true;
}