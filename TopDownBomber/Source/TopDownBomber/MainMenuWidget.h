// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNBOMBER_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	UFUNCTION(BlueprintCallable)
	void HostGame();
	
	UFUNCTION(BlueprintCallable)
	void JoinGame(FString IP);
};
