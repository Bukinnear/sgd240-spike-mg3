// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TopDownBomber : ModuleRules
{
	public TopDownBomber(TargetInfo Target)
	{
	    MinFilesUsingPrecompiledHeaderOverride = 1;
	    bFasterWithoutUnity = true;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG" });

	    // Uncomment if you are using Slate UI
	    PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
    }
}
