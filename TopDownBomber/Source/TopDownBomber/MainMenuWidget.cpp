// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownBomber.h"
#include "MainMenuWidget.h"


void UMainMenuWidget::HostGame()
{
	UGameplayStatics::OpenLevel(this, "TopDownExampleMap", true, "listen");
}

void UMainMenuWidget::JoinGame(FString IP)
{
	if (!IP.IsEmpty())
	{
		UGameplayStatics::OpenLevel(this, FName(*IP), true);
		//UE_LOG(LogTemp, Warning, TEXT("string is %s"), *IP)
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("string is empty, defaulting to localhost"));
		UGameplayStatics::OpenLevel(this, FName("127.0.0.1"), true);
	}
}

