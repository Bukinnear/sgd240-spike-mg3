// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TopDownBomber.h"
#include "TopDownBomberGameMode.h"
#include "TopDownBomberPlayerController.h"
#include "TopDownBomberCharacter.h"
#include "TopDownBomberGameState.h"
#include "TopDownBomberPlayerState.h"
#include "TDBGameInstance.h"

// Delete later
#include "Engine/Engine.h"

ATopDownBomberGameMode::ATopDownBomberGameMode()
{
	InitDefaultClasses();
	bStartPlayersAsSpectators = true;
	NewPlayerNumber = 0;
	bCanBeginRound = true;
	bUseSeamlessTravel = true;
}

void ATopDownBomberGameMode::InitDefaultClasses()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDownBomberPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	GameStateClass = ATopDownBomberGameState::StaticClass();
	PlayerStateClass = ATopDownBomberPlayerState::StaticClass();
}

void ATopDownBomberGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	// Give the new player a unique number
	NewPlayerNumber++;
	ATopDownBomberPlayerController* NewPlayerController = Cast<ATopDownBomberPlayerController>(NewPlayer);


	ATopDownBomberPlayerState* NewPlayerState = Cast<ATopDownBomberPlayerState>(NewPlayerController->PlayerState);
	if (NewPlayerState)
	{
		NewPlayerState->PlayerNumber = NewPlayerNumber;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("Playernumber could not be assigned to %s"), *NewPlayer->GetName()); }


	// TEMPORARY CODE DELETE THIS
	//if (GetWorld()->GetMapName().Contains("MainMenuMap"))
	//{
	//	NewPlayerController->ShowMainMenu();
	//}
	//else
	//{
	//	//ShowLoadingScreen();
	//	auto PlayerStart = FindPlayerStart(NewPlayerController);
	//	APawn* newPawn = SpawnDefaultPawnFor(NewPlayerController, PlayerStart);
	//	NewPlayerController->Possess(newPawn);
	//	NewPlayerController->RoundCountdown();
	//}	

	PostPlayerJoins();
}

int32 ATopDownBomberGameMode::GetNumCurrentPlayers()
{
	int32 PlayerCount = 0;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerActor = Iterator->Get();
		if (PlayerActor->PlayerState && !PlayerActor->PlayerState->bIsSpectator)
		{
			PlayerCount++;
		}
	}
	return PlayerCount;
}

void ATopDownBomberGameMode::PostSeamlessTravel()
{
	Super::PostSeamlessTravel();
	
	PostPlayerJoins();
}

void ATopDownBomberGameMode::PostPlayerJoins()
{
	if (bCanBeginRound && GetNumSpectators() + GetNumPlayers() >= 2)
	{
		FTimerHandle CountDownTimer;
		GetWorldTimerManager().SetTimer(CountDownTimer, this, &ATopDownBomberGameMode::BeginCountdownOnAllPCs, 2.0f);
	}
}

void ATopDownBomberGameMode::TravelToNextMap()
{
	RemoveWidgetsFromAllPCs();
	Cast<UTDBGameInstance>(GetWorld()->GetGameInstance())->SaveAllPersistentData();
	GetWorld()->ServerTravel("Level2");
}

void ATopDownBomberGameMode::OnPlayerDeath()
{
	if (GetNumCurrentPlayers() < 2)
	{
		auto WinningPlayerNumber = Cast<ATopDownBomberPlayerState>(GetFirstLivingPlayer()->PlayerState)->PlayerNumber;
		if (WinningPlayerNumber)
		{
			ShowWinnerScreenOnAllPCs(WinningPlayerNumber);
			GetWorldTimerManager().SetTimer(WinnerScreenDelay, this, &ATopDownBomberGameMode::TravelToNextMap, 3.0f);
		}
		else { UE_LOG(LogTemp, Error, TEXT("Win screen could not be displayed"));  }
	}
}

APlayerController* ATopDownBomberGameMode::GetFirstLivingPlayer()
{
	int32 PlayerCount = 0;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerActor = Iterator->Get();
		if (PlayerActor->PlayerState && !PlayerActor->PlayerState->bIsSpectator)
		{
			return PlayerActor;
		}
	}
	return nullptr;
}

void ATopDownBomberGameMode::BeginCountdownOnAllPCs()
{
	Cast<UTDBGameInstance>(GetWorld()->GetGameInstance())->LoadAllPersistentData();

	FConstPlayerControllerIterator ControllerIterator = GetWorld()->GetPlayerControllerIterator();

	if (ControllerIterator->IsValid())
	{
		while (ControllerIterator.operator bool())
		{
			//APlayerController* CurrentController = ControllerIterator->Get();
			ATopDownBomberPlayerController* CurrentController = Cast<ATopDownBomberPlayerController>(ControllerIterator->Get());
			if (CurrentController->IsValidLowLevel())
			{
				CurrentController->RoundCountdown();
				auto PlayerStart = FindPlayerStart(CurrentController);
				APawn* newPawn = SpawnDefaultPawnFor(CurrentController, PlayerStart);
				CurrentController->Possess(newPawn);

				++ControllerIterator;
			}
		}
	}
}

void ATopDownBomberGameMode::RemoveWidgetsFromAllPCs()
{
	FConstPlayerControllerIterator ControllerIterator = GetWorld()->GetPlayerControllerIterator();

	if (ControllerIterator->IsValid())
	{
		while (ControllerIterator.operator bool())
		{
			ATopDownBomberPlayerController* CurrentController = Cast<ATopDownBomberPlayerController>(ControllerIterator->Get());

			if (CurrentController->IsValidLowLevel())
			{
					CurrentController->RemoveAllWidgets();

				++ControllerIterator;
			}
		}
	}
}

void ATopDownBomberGameMode::ShowWinnerScreenOnAllPCs(int WinningPlayerNumber)
{
	FConstPlayerControllerIterator ControllerIterator = GetWorld()->GetPlayerControllerIterator();

	if (ControllerIterator->IsValid())
	{
		while (ControllerIterator.operator bool())
		{
			//APlayerController* CurrentController = ControllerIterator->Get();
			ATopDownBomberPlayerController* CurrentController = Cast<ATopDownBomberPlayerController>(ControllerIterator->Get());
			if (CurrentController->IsValidLowLevel())
			{
				CurrentController->ShowWinnerScreen(WinningPlayerNumber);

				++ControllerIterator;
			}
		}
	}
}

void ATopDownBomberGameMode::Debug()
{
	UE_LOG(LogTemp, Warning, TEXT("GameMode Debug Called"));
}

void ATopDownBomberGameMode::Debug2()
{
	UE_LOG(LogTemp, Warning, TEXT("Debug2 Called"));

	Cast<UTDBGameInstance>(GetGameInstance())->SaveAllPersistentData();
}