// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TopDownBomber.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TopDownBomber, "TopDownBomber" );

DEFINE_LOG_CATEGORY(LogTopDownBomber)
 