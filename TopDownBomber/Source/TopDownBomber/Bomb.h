// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

UCLASS()
class TOPDOWNBOMBER_API ABomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomb();

	UPROPERTY()
	AActor* DamageCauseActor;

protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;

	// Our static mesh
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="StaticMeshAssets")
	UStaticMeshComponent* BombMesh;

	// Particle to spawn on explosion
	UPROPERTY()
	UParticleSystem* ExplosionParticleSystem;

	// Timer used to destroy this actor after a delay
	UPROPERTY()
	FTimerHandle DelayedDestroyTimer;

private:
	// Called On BeginPlay
	UFUNCTION()
	void StartFuse();

	// Called when the fuse timer expires
	UFUNCTION()
	void FuseExpired();

	// Spawn the explosion particles
	UFUNCTION(NetMulticast, Unreliable)
	void SpawnExplosionParticles();

	// Deals damage to actors in an area
	UFUNCTION(Server, Reliable, WithValidation)
	void ApplyDamage();	

	// Delayed destruction of this actor
	UFUNCTION()
	void DelayedDestroy();

};