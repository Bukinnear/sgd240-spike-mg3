// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "TopDownBomberCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownBomberCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATopDownBomberCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:
	/** Navigate player to the given world location. */
	UFUNCTION()
	void SetNewMoveDestination(const FVector TargetLocation);

	/** Drops a Bomb at the Character's location */
	UFUNCTION(Server, Reliable, WithValidation)
	void DropBomb();

	UFUNCTION(Server, Reliable, WithValidation)
	void AddScore(int ScoreToAdd);

protected:

	// Movement related variables
	bool bMoveToDestination;
	FVector Destination;

	UPROPERTY(Replicated)
	int BombCount;

	// Set up components
	UFUNCTION()
	void InitComponents();

	/** Move the player towards the destination */
	UFUNCTION()
	void MoveToDestination();

	/** Handle OnTakeAnyDamage */
	UFUNCTION()
	void OnTakeDamage(AActor* Unknown, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser);

	UFUNCTION(Server, Reliable, WithValidation)
	void Die();
};

