// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownBomber.h"
#include "Bomb.h"
#include "GameFramework/DamageType.h"

ABomb::ABomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Replicate this actor
	bReplicates = true;

	// Create & populate our static mesh component
	BombMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BombMesh"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshOb_Sphere(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));
	if (StaticMeshOb_Sphere.Object) { BombMesh->SetStaticMesh(StaticMeshOb_Sphere.Object); }

	// Set the material
	static ConstructorHelpers::FObjectFinder <UMaterial> BombMaterial(TEXT("Material'/Game/StarterContent/Materials/M_Metal_Steel.M_Metal_Steel'"));	
	if (BombMaterial.Object) { BombMesh->SetMaterial(0, BombMaterial.Object);  }

	// Set the exposion particle
	ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionParticleAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	if (ExplosionParticleAsset.Object)	{ ExplosionParticleSystem = ExplosionParticleAsset.Object; }

	// Set the mesh to be the root component
	RootComponent = BombMesh;

	// Set the scale of this object
	RootComponent->SetWorldScale3D(FVector(.75, .75, .75));

	// Set collision channels
	BombMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	BombMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Overlap);
	BombMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
}

void ABomb::BeginPlay()
{
	Super::BeginPlay();

	// Light the fuse
	StartFuse();	
}

void ABomb::StartFuse()
{
	// Start a fuse timer & trigger FuseExpired() what it ends
	FTimerHandle FuseTimer;
	GetWorldTimerManager().SetTimer(FuseTimer, this, &ABomb::FuseExpired, 3.0f);
}

void ABomb::FuseExpired()
{	
	SpawnExplosionParticles();

	if (HasAuthority())
	{
		// Do damage to affected actors
		ApplyDamage();
	}
	// Delay this actor's destruction to avoid problems with spawning the particles
	DelayedDestroy();
}

void ABomb::SpawnExplosionParticles_Implementation()
{	
	// Get the location & rotation of our particle
	FVector ExplosionLocation = GetActorLocation();
	FRotator ExplosionRotation = GetActorRotation();
	// Spawn the particle
	UParticleSystemComponent* Explosion = UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionParticleSystem, ExplosionLocation, ExplosionRotation, true);
	Explosion->SetWorldScale3D(FVector(3.0f, 3.0f, 3.0f));
}

void ABomb::ApplyDamage_Implementation()
{	
	// if this is on the server
	if (HasAuthority())
	{
		// information for dealing damage
		AActor* ContextObject = this;
		float Damage = 25.0f;
		FVector DamageOriginPoint = GetActorLocation();
		float DamageRadius = 200;		
		TArray<AActor*> IgnoreActors; // Blank list of actors to ignore
		AActor* DamageCauser = DamageCauseActor;
		AController* InstigatorController = GetInstigatorController();
		bool NoDamageFalloff = true;
		ECollisionChannel CollisionChannel = ECC_Pawn;

		// Applies damage to anything in a radius that blocks the ECC_Pawn collision channel
		UGameplayStatics::ApplyRadialDamage(ContextObject, Damage, DamageOriginPoint, DamageRadius, UDamageType::StaticClass(), IgnoreActors, DamageCauser, InstigatorController, NoDamageFalloff, CollisionChannel);
	}
}

bool ABomb::ApplyDamage_Validate()
{
	return true;
}

void ABomb::DelayedDestroy()
{
	// If this function has called itself from a timer, destroy this actor
	if (GetWorldTimerManager().IsTimerActive(DelayedDestroyTimer))
	{
		Destroy();
	}
	// Hide this actor in game, and call this function on a timer to destroy it with a delay
	else 
	{
		SetActorHiddenInGame(true);
		GetWorldTimerManager().SetTimer(DelayedDestroyTimer, this, &ABomb::DelayedDestroy, 3.0f);
	}	
}
