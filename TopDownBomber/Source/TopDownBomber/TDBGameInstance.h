// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "TopDownBomberPlayerState.h"
#include "TDBGameInstance.generated.h"

USTRUCT()
struct FPlayerDataStruct
{
	GENERATED_BODY()

	UPROPERTY()
		int32 PlayerId;

	UPROPERTY()
		int PlayerNumber;

	UPROPERTY()
		int PlayerScore;

	FPlayerDataStruct()
	{
		PlayerId = 0;
		PlayerNumber = 0;
		PlayerScore = 0;
	}

	FPlayerDataStruct(ATopDownBomberPlayerState* PlayerState)
	{

		PlayerId = PlayerState->PlayerId;
		PlayerNumber = PlayerState->PlayerNumber;
		PlayerScore = PlayerState->PlayerScore;
	}
};

/**
 * 
 */
UCLASS()
class TOPDOWNBOMBER_API UTDBGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UFUNCTION()
		virtual void SaveAllPersistentData();

	UFUNCTION()
		virtual void LoadAllPersistentData();
	
protected:
	TArray<FPlayerDataStruct> PlayerDataArray;
};
