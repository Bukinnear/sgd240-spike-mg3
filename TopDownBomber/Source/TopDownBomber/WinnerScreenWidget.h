// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "WinnerScreenWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNBOMBER_API UWinnerScreenWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintImplementableEvent)
		void SetWinningPlayerNumber(int WinningPlayerNumber);	
	
};
