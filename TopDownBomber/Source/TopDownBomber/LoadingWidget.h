// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "LoadingWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNBOMBER_API ULoadingWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void MatchIsStarting(bool IsStarting);

	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void SetCountdownText(int CountdownIndex);	
	
};
