---
title: Spike Report
---

Persistence across levels
=========================

Introduction
------------

We have some basic multiplayer now, but the next step is for us to be
able to preserve player data on the server!

To start with, we’re going to upgrade our game to have multiple maps,
and allow for us to switch between them.

Goals
-----

Building on Spike MG-2

1.  Create a second level for the players to navigate around

    a.  Ensure that everything works as in MG-2

2.  Enable [Seamless
    Travel](https://docs.unrealengine.com/latest/INT/Gameplay/Networking/Travelling/index.html)
    between the two different levels, when a winner is declared…

    a.  All players should remain connected

    b.  Coin score should be kept for current round and accumulated
        across rounds

    c.  The amount of times that a given player has “won” should be
        counted, and preserved across the game.

    d.  Update Main Menu with a Name entry, which is given to the server

    e.  Update in-game UI to display Player Name, Current Round Coins,
        Total Coins Collected, Times Won

3.  Allow for players to drop in and out, with their client reconnecting
    and resuming any score which was held.

    a.  Allow for 4+ players (i.e. if player A drops out, then player B
        joins in the 4^th^ slot, player A can re-join when another
        player drops out and keep their score/win count. Use the
        client’s IP address as a token to identify players.

    b.  Allow &gt;4^th^ player as spectators

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K,   Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

-   Travelling in Multiplayer
    <https://docs.unrealengine.com/latest/INT/Gameplay/Networking/Travelling/index.html>

-   Set Input Mode in C++
    <https://forums.unrealengine.com/development-discussion/c-gameplay-programming/106709-how-to-set-input-mode-in-c>

-   Data persistence using CopyProperties
    <https://answers.unrealengine.com/questions/221460/how-can-i-use-seamless-travel-to-persist-data-acro.html>

-   Structs and how to use them
    <https://wiki.unrealengine.com/Structs,_USTRUCTS(),_They%27re_Awesome>

Tasks Undertaken
----------------

### Score

1.  For this project, the scoring wasn’t properly set up in the previous
    spike, I implemented that here:

    When a character is killed, that character sends an RPC to the
    server to add score to the PlayerState belonging to the player that
    killed them.

### UI

See how to set the input mode of the cursor in C++ [here](#ui-1)

#### Loading Screen

1.  For the UI, when a round begins, the GameMode iterates through each
    of the PlayerControllers using

    GetWorld()-&gt;GetPlayerControllerIterator()

    And sends a client RPC to show a loading screen.

#### Winner Screen

1.  When a player dies, the GameMode checks for how many players are
    left that are not spectators (See
    [here](#playercontrollers-and-rpcs) for details on how to do that)

2.  If there are less than 2 players, show a winner screen with the last
    remaining player

3.  Then wait 3 seconds before calling seamless travel to the next map.

### Seamless Travel

1.  See
    [here](https://docs.unrealengine.com/latest/INT/Gameplay/Networking/Travelling/index.html)
    for how to seamlessly travel, specifically, we will be using
    UWorld::ServerTravel

2.  This will be called on the server after a winner is detected and
    declared.

### Persistent Data

1.  There are two methods of persisting data across maps, see
    [here](#persistent-data) for details.

    a.  This project used the GameInstance variant, although either
        would work here.

2.  Data must be saved before travelling and loaded once seamless travel
    has completed.

    a.  Note that GameModeBase::PostLogin is not called following
        seamless travel. You can use GameModeBase::PostSeamlessTravel
        instead.

What We Found Out
-----------------

### UI

To set the input mode of the cursor in C++. Use SetInputMode in the
PlayerController.

If you just want to quickly set the input mode without changing any of
the default settings, you can use SetInputMode(FInputModeGameAndUI());

Otherwise, you will need to create a new FInputModeDataBase object,
which is further divided into 3 sub classes – FinputModeGameAndUI,
FInputModeUIOnly, and FInputModeGameOnly.

For example:

FInputModeGameAndUI InputMode;

InputMode.SetHideCursorDuringCapture(false);

SetInputMode(InputMode);

For this project, it is important to set the SetHideCursorDuringCapture
to false, to avoid locking the mouse in one place onscreen while moving.

### PlayerControllers and RPCs

You can use Client or Server RPCs in the PlayerController, but not
Multicast, as PlayerControllers only exist on the server and their
owning client.

This is very useful for calling functions that need to be synchronized
across clients, such as loading screens, or beginning the round
countdown.

### Getting the number of Players

The GameMode has two functions for getting the number of players in the
game:

-   GetNumPlayers()

    -   This will get the all of the players in the game that are not
        limited strictly to being spectators via bOnlySpectator in the
        PlayerState,

        -   **NOTE:** This includes players that are *currently*
            spectators

-   GetNumSpectators()

    -   This will get all players in the game that *are* limited
        strictly to being spectators via bOnlySpectator in the
        PlayerState

For this project, I created a new function similar to GetNumPlayers
called GetNumCurrentPlayers that excludes players that are *currently*
spectators.

### Persistent Data

There are 2 main ways to persist data across map changes in UE4:

1.  Save & load data from the GameInstance (as it persists across the
    lifetime of the program)

    a.  To do this, the easiest way is to use a
        [Struct](https://wiki.unrealengine.com/Structs,_USTRUCTS(),_They%27re_Awesome)
        to save a set of properties to an array

    b.  The PlayerState has a unique PlayerId that persists across map
        changes (via CopyProperties) that can be used to match the data
        to the player.

2.  Use CopyProperties in the PlayerState

    a.  To do this, see
        [here](https://answers.unrealengine.com/questions/221460/how-can-i-use-seamless-travel-to-persist-data-acro.html)
        or look at the source code.

    b.  Note that this method can only be used to persist public
        properties in the PlayerState.

This is beyond the scope of this project, but another way is to save and
load data to a file instead.

If you are using an On\_Rep function in the playerstate,
[see](#on_rep-race-conditions-in-the-playerstate) here for cautionary
details on doing that.

\[Optional\] Open Issues/risks
------------------------------

### On\_Rep Race conditions in the PlayerState

I had some trouble using an On\_Rep function in the playerstate to
update the player’s score UI – eventually I have to use a Boolean to
limit it from firing until the round had begun, to avoid access
violations.

### Unacomplished Goals

Due to time constraints, not all of the goals could be completed for this spike, the following will need to be revised at a later date: 

    d.  Update Main Menu with a Name entry, which is given to the server

    e.  Update in-game UI to display Player Name, Current Round Coins,
        Total Coins Collected, Times Won

3.  Allow for players to drop in and out, with their client reconnecting
    and resuming any score which was held.

    a.  Allow for 4+ players (i.e. if player A drops out, then player B
        joins in the 4^th^ slot, player A can re-join when another
        player drops out and keep their score/win count. Use the
        client’s IP address as a token to identify players.

    b.  Allow &gt;4^th^ player as spectators

For returning players, it would need to build upon the data saved to the GameInstance, and several details such as PlayerNames, and rounds won would be a similar process.

I am unsure if the PlayerId in the PlayerState would persist on
disconnecting and reconnecting, and would need to be investigated.
